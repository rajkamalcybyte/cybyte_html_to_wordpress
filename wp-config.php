<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

 
define('WP_HOME','http://localhost/cybytewp/');
define('WP_SITEURL','http://localhost/cybytewp/');

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'cybytewp');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'uQsq7Eqk4{?SFWY+P)?BMaN2Kz`dyuXXbXn7oG4 RP])--&3FO1FFdQN|gXZ@PX8');
define('SECURE_AUTH_KEY',  ']AiG2eJuKM9bM/HK_] ?Hh.O6eaVQ_+/*ErdCkCv|l8<1b?e3S1`KYP, r+{4yhH');
define('LOGGED_IN_KEY',    'nSJsgCS^~IiwtZ FN:Ks5D3g*U5)SjXckYQFq#v;(q$,eKSA$l{FGE*G;<VTdKy7');
define('NONCE_KEY',        'C(#8:DFlz#C{wV^!Q>tJJlC{@h6b6tYP(-;&$n9k0J{^C69`&SGhGG/QFhf_.Qu$');
define('AUTH_SALT',        'q5l@;W>ZK6xsYS6nfp ol5,;fw<JYYY`u*-=aKrx$sJp+[dl@8#ThCex]&G4)M3I');
define('SECURE_AUTH_SALT', 'j/tsu|8/f<3AAb~7nD?wL<]59GyIKB@qJZY2 Jq*cVucb[c[ejv(NPdqr}[Bg<s{');
define('LOGGED_IN_SALT',   'p*?^}Jp(>ZV(INOT>*;V*D/nO`)^2X!74}IgYy+BJf*1sXXo,ZO9wU90E1pma<5i');
define('NONCE_SALT',       'iueTKUaHzAB R{[lNy##HJ*wWle.;mi#NZ+7`pB^r1{i5l^dpx79`gqs886vw9~?');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';
define( 'ALLOW_UNFILTERED_UPLOADS', true );
/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
