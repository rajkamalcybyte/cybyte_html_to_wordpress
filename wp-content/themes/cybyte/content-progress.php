<section id="progress">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 pregress-container">
                <div class="row">
                    <div class="col-sm-5">
                        <div class="progress-art-work">
                            <img src="<?php echo $what_we_do_main_image['url']; ?>" class="img-responsive">
                        </div>
                    </div>
                    <div class="col-sm-6 col-sm-offset-1">
                        <img src="<?php bloginfo('stylesheet_directory'); ?>/assets/img/blueDots.png">
                        <h2><?php echo $what_we_do_heading; ?></h2>
                        <?php
                        // The Query
                        $query1 = new WP_Query(array('post_type' => 'what_we_do', 'orderby' => 'date', 'order' => 'ASC'));

                        if ($query1->have_posts()) {
                            // The Loop
                            while ($query1->have_posts()) {
                                $query1->the_post();
                                $heading = get_post_meta(get_the_ID(), 'heading', true);
                                $image_obj = get_field('image');
                                $image_url = $image_obj['url'];
                                $body = get_post_meta(get_the_ID(), 'body', true);

                                ?>
                                <div class="clearfix"></div>
                                <div class="media">
                                    <div class="media-left">
                                        <a href="#"><img src="<?php echo $image_url; ?>" class="progress-img"> </a>
                                    </div>
                                    <div class="media-body">
                                        <h4 class="media-heading"><?php echo $heading; ?></h4>
                                        <p><?php echo $body; ?></p>
                                    </div>
                                </div>
                                <?php
                            }
                        }
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>