<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package cybyte
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="http://gmpg.org/xfn/11">

    <!--    <link rel="apple-touch-icon" href="apple-touch-icon.png">-->
    <link rel="stylesheet" href="<?php bloginfo('stylesheet_directory'); ?>/assets/css/bootstrap.min.css">
    <link href="<?php bloginfo('stylesheet_directory'); ?>/assets/vendor/owl/assets/owl.carousel.min.css"
          rel="stylesheet"/>
    <link href="<?php bloginfo('stylesheet_directory'); ?>/assets/vendor/owl/assets/owl.theme.default.min.css"
          rel="stylesheet"/>
    <link href="<?php bloginfo('stylesheet_directory'); ?>/assets/vendor/superslides/stylesheets/superslides.css"
          rel="stylesheet" type="text/css"/>
    <script src="<?php bloginfo('stylesheet_directory'); ?>/assets/js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>

    <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

<!--[if lt IE 8]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade
    your browser</a> to improve your experience.</p>
<![endif]-->
<div class="topline navbar-fixed-top">
    <div class="col-xs-5 orange-line"></div>
    <div class="col-xs-7 blue-line"></div>
</div>
<div class="clearfix"></div>
<!-- Navigation -->
<nav class="navbar navbar-custom navbar-fixed-top" role="navigation">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-main-collapse">
                Menu <i class="fa fa-bars"></i>
            </button>
            <a class="navbar-brand page-scroll" href="<?php echo home_url();?>">
                <img src="<?php echo(get_header_image()); ?>" alt="logo">
            </a>
        </div>
        <?php
        wp_nav_menu(array(
            'theme_location' => 'primary',
            'container' => 'nav',
            'container_class' => 'collapse navbar-collapse navbar-right navbar-main-collapse',
            'menu_class' => 'nav navbar-nav',
        ));
        ?>
    </div>
    <!-- /.container -->
</nav>

