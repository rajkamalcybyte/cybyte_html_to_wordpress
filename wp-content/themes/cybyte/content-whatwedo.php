<?php
/**
 * Created by PhpStorm.
 * User: Kamal
 * Date: 08-12-2017
 * Time: 17:07
 */
?>
<!-- section whatwedo -->
<section id="whatwedo">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 text-center">
                <img src="<?php bloginfo('stylesheet_directory'); ?>/assets/img/blueDots.png">
                <h2>WHAT WE DO</h2>
            </div>
            <div class="col-sm-10 col-md-10 col-md-offset-1 col-sm-offset-1">
                <!-- <what we do carousel> -->
                <div class="carouselSection">
                    <div class="owl-carousel owl-theme" id="whatwedoCarou">

                        <?php
                        // The Query
                        $query1 = new WP_Query(array('post_type' => 'what_we_do_main_page', 'orderby' => 'date', 'order' => 'ASC'));

                        if ($query1->have_posts()) {
                            // The Loop
                            while ($query1->have_posts()) {
                                $query1->the_post();
                                $heading = get_post_meta(get_the_ID(), 'heading', true);
                                $image_obj = get_field('icon');
                                $image_url = $image_obj['url'];
                                $heading = get_post_meta(get_the_ID(), 'heading', true);
                                $body = get_post_meta(get_the_ID(), 'body', true);
                                ?>

                                <div class="item">
                                    <div class="eachDo">
                                        <div class="icon">
                                            <img src="<?php echo $image_url; ?>">
                                        </div>
                                        <div class="h3"> <?php echo $heading; ?> </div>
                                        <p> <?php echo $body; ?> </p>
                                        <a href="#" class="btn btn-rounded btn-blue-line-rounded"> read more</a>
                                    </div>
                                </div>
                                <?php
                            }
                        }
                        ?>

                    </div>
                </div>
                <!-- <what we do carousel> -->
            </div>
        </div>
    </div>
</section>