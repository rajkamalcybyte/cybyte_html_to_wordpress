/// jQuery to collapse the navbar on scroll
function collapseNavbar() {
    if ($(".navbar").offset().top > 50) {
        $(".navbar-fixed-top").addClass("top-nav-collapse");
    } else {
        $(".navbar-fixed-top").removeClass("top-nav-collapse");
    }
}

$(window).scroll(collapseNavbar);
$(document).ready(collapseNavbar);


$('#slides').superslides({
  animation: 'fade',
  hashchange: true,
  play: 3000,
  hashchange: false
});


$('#whatwedoCarou').owlCarousel({
    loop:true,
    margin:0,
    navText : ["<div class='prev'></div>","<div class='next'></div>"],
    responsiveClass:true,
    dots: false,
    nav: true,
    
    responsive:{
        0:{
            items:1,
            nav:false,
            autoplay:5000
        },
       768:{
            items:2,
            nav:false
        },
        1000:{
            items:3,
            nav:true,
            autoplay:5000

            
        }
    }
});





$('#partnerList').owlCarousel({
    loop:true,
    margin:10,
    autoplay: 3000,
    responsiveClass:true,
    dots: false,
    responsive:{
        0:{
            items:2,
            nav:true
        },
        600:{
            items:3,
            nav:false
        },
        1000:{
            items:5,
            nav:true,
            loop:false
        }
    }
});



$('#testi').owlCarousel({
    loop:false,
    margin:10,
    responsiveClass:true,
    dots: true,
    items: 1,
    nav: false

});




// // jQuery for page scrolling feature - requires jQuery Easing plugin
// $(function() {
//     $('a.page-scroll').bind('click', function(event) {
//         var $anchor = $(this);
//         $('html, body').stop().animate({
//             scrollTop: $($anchor.attr('href')).offset().top
//         }, 1500, 'easeInOutExpo');
//         event.preventDefault();
//     });
// });

// // Closes the Responsive Menu on Menu Item Click
// $('.navbar-collapse ul li a').click(function() {
//     $(this).closest('.collapse').collapse('toggle');
// });

