<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package cybyte
 */

get_header(); ?>
    <div class="page-title">
    <section class="breadcrumbs parallax-window text-center" data-parallax="scroll" data-image-src="<?php bloginfo('stylesheet_directory'); ?>/assets/img/brad2.jpg">
        <h2>Blog</h2>
        <div class="container">
            <div class="row">
                <div class="col-sm-8 col-sm-offset-2">
                    <ol class="breadcrumb  breadcrumbs-path">
                        <li><a href="#">Home</a></li>
                        <li class="active">Blog</li>
                    </ol>
                </div>
            </div>
    </section>
    </div>
    <div class="container">
        <div class="row">
            <section class="col-md-9" id="blog">
                <?php
                		if ( have_posts() ) :

                			if ( is_home() && ! is_front_page() ) : ?>
                				<header>
                					<h1 class="page-title screen-reader-text"><?php single_post_title(); ?></h1>
                				</header>

                			<?php
                			endif;

                			/* Start the Loop */
                			while ( have_posts() ) : the_post();

                				/*
                				 * Include the Post-Format-specific template for the content.
                				 * If you want to override this in a child theme, then include a file
                				 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
                				 */
                				get_template_part( 'template-parts/content', get_post_format() );

                			endwhile;
                		else :

                			get_template_part( 'template-parts/content', 'none' );

                		endif; ?>
                <nav class="pagination">
                    <?php
                    function pagination_bar() {
                        global $wp_query;

                        $total_pages = $wp_query->max_num_pages;

                        if ($total_pages > 1){
                            $current_page = max(1, get_query_var('paged'));

                            echo paginate_links(array(
                                'base' => get_pagenum_link(1) . '%_%',
                                'format' => '/page/%#%',
                                'current' => $current_page,
                                'total' => $total_pages,
                                'type' => 'list',
                            ));
                        }
                    }


                    pagination_bar(); ?>
                </nav>
            </section>
            <aside class="col-md-3">
               <?php get_sidebar(); ?>
            </aside>
        </div>
    </div>


<?php

get_footer();
