<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package cybyte
 */

get_header(); ?>
    <div class="page-title">
        <section class="breadcrumbs parallax-window text-center" data-parallax="scroll" data-image-src="<?php bloginfo('stylesheet_directory'); ?>/assets/img/brad2.jpg">
            <h2>Blog</h2>
            <div class="container">
                <div class="row">
                    <div class="col-sm-8 col-sm-offset-2">
                        <ol class="breadcrumb  breadcrumbs-path">
                            <li><a href="#">Home</a></li>
                            <li class="active">Blog</li>
                        </ol>
                    </div>
                </div>
        </section>
    </div>
    <div class="container">
        <div class="row">
            <section class="col-md-9" id="blog">
                <?php
                while ( have_posts() ) : the_post();

                    get_template_part( 'template-parts/content', get_post_type() );

                    the_post_navigation();

                    // If comments are open or we have at least one comment, load up the comment template.
                    if ( comments_open() || get_comments_number() ) :
                        comments_template();
                    endif;

                endwhile; // End of the loop.
                ?>
            </section>
            <aside class="col-md-3">
                <?php get_sidebar(); ?>
            </aside>
        </div>
    </div>


<?php

get_footer();
