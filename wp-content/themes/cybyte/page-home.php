<?php
/**
 * Template Name: Home Page
 */

$home_content_image = get_field('home_content_image');
$home_content_about_cybyte = get_field('home_content_about_cybyte');
$home_content_read_more_link = get_field('home_content_read_more_link');

$call_to_action_text = get_field('call_to_action_text');
$call_to_action_link = get_field('call_to_action_link');

$what_we_do_heading = get_field('what_we_do_heading');
$what_we_do_main_image = get_field('what_we_do_main_image');

get_header(); ?>
    <header class="intro">
        <div class="intro-body">
            <div id="slides">
                <ul class="slides-container">
                    <li>
                        <img src="<?php bloginfo('stylesheet_directory'); ?>/assets/img/banner-1.jpg" alt="cybyte">
                        <div class="container">
                            <div class="h1">Unstructured Data is Everywhere.</div>
                            <h4>Let SMAC data work for you. Social, mobile, analytics and cloud, with open source
                                chaser.<br/>
                                That's CyByte Digital.</h4>
                            <a href="" class="btn btn-default btn-rounded btn-orange"> Read More </a>
                        </div>
                    </li>
                    <li>
                        <img src="<?php bloginfo('stylesheet_directory'); ?>/assets/img/banner-1.jpg" alt="cybyte">
                        <div class="container">
                            <div class="h1">Unstructured Data is Everywhere.</div>
                            <h4>Let SMAC data work for you. Social, mobile, analytics and cloud, with open source
                                chaser.<br/>
                                That's CyByte Digital.</h4>
                            <a href="" class="btn btn-default btn-rounded btn-orange"> Read More </a>
                        </div>
                    </li>
                    <li>
                        <img src="<?php bloginfo('stylesheet_directory'); ?>/assets/img/banner-1.jpg" alt="cybyte">
                        <div class="container">
                            <div class="h1">Unstructured Data is Everywhere.</div>
                            <h4>Let SMAC data work for you. Social, mobile, analytics and cloud, with open source
                                chaser.<br/>
                                That's CyByte Digital.</h4>
                            <a href="" class="btn btn-default btn-rounded btn-orange"> Read More </a>
                        </div>
                    </li>
                </ul>
                <nav class="slides-navigation">
                    <a href="#" class="next"><i class="fa fa-angle-right" aria-hidden="true"></i></a>
                    <a href="#" class="prev"><i class="fa fa-angle-left" aria-hidden="true"></i></a>
                </nav>
            </div>
        </div>
    </header>
    <section id="homeContent">
        <div class="container">
            <!-- Example row of columns -->
            <div class="row">
                <div class="col-md-6">
                    <img src="<?php echo $home_content_image['url']; ?>" class="img-responsive" alt="Cybyte">
                </div>
                <div class="col-md-6">
                    <div class="cybyteLogo">
                        <img src="<?php echo(get_header_image()); ?>" width="180">
                    </div>
                    <p><?php echo $home_content_about_cybyte; ?></p>
                    <p><a class="bt-ornage-link" href="#" role="button"><span class="colorBadge"></span> Read More</a>
                    </p>
                </div>
            </div>
    </section>

<?php get_template_part('content', 'whatwedo'); ?>

<?php get_template_part('content', 'howwedo'); ?>

    <section id="callToAction">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 text-center">
                    <h3><?php echo $call_to_action_text; ?></h3>
                    <a href="<? echo $call_to_action_link; ?>" class="btn btn-rounded btn-white-line-rounded btn-lg">Contact
                        Us</a>
                </div>
            </div>
        </div>
    </section>

<?php get_template_part('content', 'ourclientsay'); ?>
<?php
get_footer();
