<section id="OurclientSay">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 text-center">
                <img src="<?php bloginfo('stylesheet_directory'); ?>/assets/img/OrangeDots.png">
                <h2>Our client say</h2>
                <img src="<?php bloginfo('stylesheet_directory'); ?>/assets/img/qu.png">
            </div>
            <div class="col-sm-10 col-md-8 col-md-offset-2 col-sm-offset-1">
                <!-- <what we do carousel> -->
                <div class="carouselSection">
                    <div class="owl-carousel owl-theme" id="testi">
                        <?php

                        // The Query
                        $query1 = new WP_Query(array('post_type' => 'our_client_say', 'orderby' => 'date', 'order' => 'ASC'));

                        if ($query1->have_posts()) {
                            // The Loop
                            while ($query1->have_posts()) {
                                $query1->the_post();
                                $context = get_post_meta(get_the_ID(), 'context', true);
                                $person_name = get_post_meta(get_the_ID(), 'person_name', true);
                                $company_name = get_post_meta(get_the_ID(), 'company_name', true);
                                echo "<div class='item'>
                                        <div class='eachTestimonial'>
                                            <p class='cotext'>$context</p>
                                            <p class='personName'>$person_name</p>
                                            <small>$company_name</small>
                                        </div>
                                        </div>";
                            }

                            /* Restore original Post Data
                            * NB: Because we are using new WP_Query we aren't stomping on the
                            * original $wp_query and it does not need to be reset with
                            * wp_reset_query(). We just need to set the post data back up with
                            * wp_reset_postdata().
                            */
                            wp_reset_postdata();
                        }

                        ?>

                    </div>
                </div>
                <!-- <what we do carousel> -->
            </div>
        </div>
    </div>
</section>