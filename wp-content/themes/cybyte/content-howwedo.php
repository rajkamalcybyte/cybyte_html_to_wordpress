<section id="howwedo">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 text-center">
                <img src="<?php bloginfo('stylesheet_directory'); ?>/assets/img/OrangeDots.png">
                <h2>How We do</h2>
            </div>
            <?php
            // The Query
            $query1 = new WP_Query(array('post_type' => 'how_we_do', 'orderby' => 'date', 'order' => 'ASC'));

            if ($query1->have_posts()) {
                // The Loop
                while ($query1->have_posts()) {
                    $query1->the_post();
                    $heading = get_post_meta(get_the_ID(), 'heading', true);
                    $image_obj = get_field('image');
                    $image_url = $image_obj['url'];
                    $heading_sub = get_post_meta(get_the_ID(), 'heading_sub', true);
                    $content_list = get_post_meta(get_the_ID(), 'content_list', true);
                    $read_more_url = get_post_meta(get_the_ID(), 'read_more_url', true);
                    ?>
                    <div class="col-sm-4">
                        <div class="wedoblock  text-center">

                            <h3><?php echo $heading; ?> </h3>
                            <figure>
                                <img src="<?php echo $image_url; ?>">
                            </figure>
                            <div class="h3"><?php echo $heading_sub; ?></div>
                            <ul class="list-unstyled">
                                <?php echo $content_list; ?>
                            </ul>
                            <p><a href="<?php echo $read_more_url; ?>" class="btn btn-orange btn-rounded">Ream More</a>
                            </p>
                        </div>
                    </div>
                    <?php
                }
            }
            ?>
        </div>
    </div>
</section>