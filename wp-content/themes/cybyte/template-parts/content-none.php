<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package cybyte
 */

get_header(); ?>
<div class="page-title">
    <section class="breadcrumbs parallax-window text-center" data-parallax="scroll" data-image-src="<?php bloginfo('stylesheet_directory'); ?>/assets/img/brad2.jpg">
        <h2>Blog</h2>
        <div class="container">
            <div class="row">
                <div class="col-sm-8 col-sm-offset-2">
                    <ol class="breadcrumb  breadcrumbs-path">
                        <li><a href="<?php echo home_url();?>">Home</a></li>
                        <li class="active">Blog</li>
                    </ol>
                </div>
            </div>
    </section>
</div>
<div class="container">
    <div class="row">
        <section class="col-md-9" id="blog">
		<?php
		if ( is_home() && current_user_can( 'publish_posts' ) ) : ?>

			<p><?php
				printf(
					wp_kses(
						/* translators: 1: link to WP admin new post page. */
						__( 'Ready to publish your first post? <a href="%1$s">Get started here</a>.', 'cybyte' ),
						array(
							'a' => array(
								'href' => array(),
							),
						)
					),
					esc_url( admin_url( 'post-new.php' ) )
				);
			?></p>

		<?php elseif ( is_search() ) : ?>

			<p><?php esc_html_e( 'Sorry, but nothing matched your search terms. Please try again with some different keywords.', 'cybyte' ); ?></p>
			<?php
				get_search_form();

		else : ?>

			<p><?php esc_html_e( 'It seems we can&rsquo;t find what you&rsquo;re looking for. Perhaps searching can help.', 'cybyte' ); ?></p>
			<?php
				get_search_form();

		endif; ?>
        </section>
    </div>
</div>


<?php

get_footer();
