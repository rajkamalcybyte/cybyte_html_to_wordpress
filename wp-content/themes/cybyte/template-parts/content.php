<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package cybyte
 */

?>
<div class="blog-item nbox">
<!--    <img src="http://placehold.it/900x400" class="img-responsive" alt="Cybyte">-->
    <?php
    if ( has_post_thumbnail() ) { // check if the post has a Post Thumbnail assigned to it.
        ?>
        <img src="<?php echo the_post_thumbnail_url(array(900,400)); ?>" class="img-responsive" alt="Cybyte">
    <?php }
    ?>
    <div class="blog-meta">


        <?php
        if ( 'post' === get_post_type() ) : ?>
        <ul class="tags list-inline">
           <?php
//            /* translators: used between list items, there is a space after the comma */
//            $tags_list = get_the_tag_list( '', esc_html_x( ', ', 'list item separator', 'cybyte' ) );
//            if ( $tags_list ) {
//            /* translators: 1: list of tags. */
//            printf( '<li>' . esc_html__( 'Tagged %1$s', 'cybyte' ) . '</li>', $tags_list ); // WPCS: XSS OK.
//            }


            /* translators: used between list items, there is a space after the comma */
            $categories_list = get_the_category_list( esc_html__( ', ', 'cybyte' ) );
            if ( $categories_list ) {
            /* translators: 1: list of categories. */
            printf( '<li  class="btn btn-orange btn-orange">' . esc_html__( '%1$s', 'cybyte' ) . '</li>', $categories_list ); // WPCS: XSS OK.
            }

            /* translators: used between list items, there is a space after the comma */
            $tags_list = get_the_tag_list( '', esc_html_x( ', ', 'list item separator', 'cybyte' ) );
            if ( $tags_list ) {
            /* translators: 1: list of tags. */
            printf( '<li class="btn btn-blue btn-blue">' . esc_html__( '%1$s', 'cybyte' ) . '</li>', $tags_list ); // WPCS: XSS OK.
            }?>
        </ul>
            <?php
        endif; ?>	<?php
        		if ( is_singular() ) :
        			the_title( '<h4>', '</h4>' );
        		else :
        			the_title( '<h4><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h4>' );
        		endif;
        		?>
<!--        <h4><a href="#">Lorem ipsum dolor sit amet consectetur adipiscing elit</a></h4>-->
<!--        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum pellentesque ipsum id tortor finibus ornare. Fusce lobortis convallis erat, maximus fringilla nulla ultrices vitae. Fusce vulputate dui eu convallis pellentesque. Sed varius magna vel est auctor lobortis.</p>-->
        <?php
        			the_content( sprintf(
        				wp_kses(
        					/* translators: %s: Name of current post. Only visible to screen readers */
        					__( 'Continue reading<span class="screen-reader-text"> "%s"</span>', 'cybyte' ),
        					array(
        						'span' => array(
        							'class' => array(),
        						),
        					)
        				),
        				get_the_title()
        			) );
        			?>
        <img src="img/social.png">
        <hr>
        <div class="row postedBy">
                    <?php
                	if ( 'post' === get_post_type() ) :
                        $time_string = '<time class="entry-date published updated" datetime="%1$s">%2$s</time>';
                        if ( get_the_time( 'U' ) !== get_the_modified_time( 'U' ) ) {
                            $time_string = '<time class="entry-date published" datetime="%1$s">%2$s</time><time class="updated" datetime="%3$s">%4$s</time>';
                        }

                        $time_string = sprintf( $time_string,
                            esc_attr( get_the_date( 'c' ) ),
                            esc_html( get_the_date() ),
                            esc_attr( get_the_modified_date( 'c' ) ),
                            esc_html( get_the_modified_date() )
                        );

                        $posted_on = sprintf(
                        /* translators: %s: post date. */
                            esc_html_x( '%s', 'post date', 'cybyte' ),
                            '<a href="' . esc_url( get_permalink() ) . '" rel="bookmark">' . $time_string . '</a>'
                        );
                        $byline = sprintf(
                        /* translators: %s: post author. */
                            esc_html_x( '%s', 'post author', 'cybyte' ),
                            '<span class="author vcard"><a class="url fn n" href="' . esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ) . '">' . esc_html( get_the_author() ) . '</a></span>'
                        );

                        ?>
                        <div class="col-xs-8"><?php echo $byline; ?></div>
                        <div class="col-xs-4 text-right date">
            			<?php echo $posted_on; ?>
            		</div><!-- .entry-meta -->
            		<?php
            		endif; ?>
        </div>
    </div>
</div>


