<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package cybyte
 */

?>


<?php wp_footer(); ?>

<footer id="preFooter">
    <div class="container">
        <div class="row">
            <?php

            // The Query
            $query1 = new WP_Query( array('post_type' => 'contact', 'orderby' => 'date', 'order' => 'ASC') );

            if ( $query1->have_posts() ) {
                // The Loop
                while ( $query1->have_posts() ) {
                    $query1->the_post();
                    $label = get_post_meta( get_the_ID(), 'label',   true );
                    $image_obj = get_field('image');
                    $image_url = $image_obj['url'];
                    echo "<div class='col-sm-12 col-md-4 text-center'>
                        <img src='$image_url'>
                        <p>$label</p>
                        </div>";
                }

                /* Restore original Post Data
                * NB: Because we are using new WP_Query we aren't stomping on the
                * original $wp_query and it does not need to be reset with
                * wp_reset_query(). We just need to set the post data back up with
                * wp_reset_postdata().
                */
                wp_reset_postdata();
            }

            ?>
<!--            <div class="col-sm-12 col-md-5 text-center">-->
<!--                <img src="--><?php //bloginfo('stylesheet_directory'); ?><!--/assets/img/location.svg">-->
<!--                <p>52 Pine Creek Rd. Ste. 120, Wexford PA 15090</p>-->
<!--            </div>-->
<!--            <div class="col-sm-6 col-md-3 text-center">-->
<!--                <img src="--><?php //bloginfo('stylesheet_directory'); ?><!--/assets/img/phone.svg">-->
<!--                <p><a href="callto:(412) 837-9299">(412) 837-9299</a></p>-->
<!--            </div>-->
<!--            <div class="col-sm-6 col-md-4 text-center">-->
<!--                <img src="--><?php //bloginfo('stylesheet_directory'); ?><!--/assets/img/mail.svg">-->
<!--                <p><a href="mailto:info@cybyte.com">info@cybyte.com</a></p>-->
<!--            </div>-->
        </div>
    </div>
</footer>
<footer class="primary">
    <div class="container">
        <div class="row">

            <div class="col-md-8">
                <?php
                wp_nav_menu( array(
                    'theme_location'    => 'footer',
                    'container'    => 'ul',
                    'menu_class'        => 'nav nav-pills',
                ) );
                ?>

            </div>
            <div class="col-md-4">
                <ul class="list-inline social-icons">
                    <?php

                    // The Query
                    $query1 = new WP_Query( array('post_type' => 'social_links', 'orderby' => 'date', 'order' => 'ASC') );

                    if ( $query1->have_posts() ) {
                        // The Loop
                        while ( $query1->have_posts() ) {
                            $query1->the_post();
                            $link = get_post_meta( get_the_ID(), 'link',   true );
                            $image_obj = get_field('image');//get_post_meta( get_the_ID(), 'image',   true );
                            $image_url = $image_obj['url'];
                            echo "<li><a href='$link'><img src='$image_url'></a></li>";
                        }

                        /* Restore original Post Data
                        * NB: Because we are using new WP_Query we aren't stomping on the
                        * original $wp_query and it does not need to be reset with
                        * wp_reset_query(). We just need to set the post data back up with
                        * wp_reset_postdata().
                        */
                        wp_reset_postdata();
                    }

                    ?>
<!--                    <li><a href="#"><img src="--><?php //bloginfo('stylesheet_directory'); ?><!--/assets/img/facebook-dreamstale25.png"></a></li>-->
<!--                    <li><a href="#"><img src="--><?php //bloginfo('stylesheet_directory'); ?><!--/assets/img/twitter-dreamstale71.png"></a></li>-->
<!--                    <li><a href="#"><img src="--><?php //bloginfo('stylesheet_directory'); ?><!--/assets/img/linkedin-dreamstale45.png"></a></li>-->
<!--                    <li><a href="#"><img src="--><?php //bloginfo('stylesheet_directory'); ?><!--/assets/img/google+-dreamstale37.png"></a></li>-->
<!--                    <li><a href="#"><img src="--><?php //bloginfo('stylesheet_directory'); ?><!--/assets/img/youtube2-dreamstale87.png"></a></li>-->
<!--                    <li><a href="#"><img src="--><?php //bloginfo('stylesheet_directory'); ?><!--/assets/img/pinterest-dreamstale57.png"></a></li>-->
                </ul>
            </div>
        </div>
    </div>
</footer>
<footer class="secondary">
    <div class="container">
        <div class="row">
            <div class="col-sm-6">
                <p>&copy; <?php echo date('Y');  ?> <?php echo bloginfo('name');?></p>
            </div>
            <div class="col-sm-6">
                <ul class="nav nav-pills">
                    <li><a href="#">Sitemap</a></li>
                    <li><a href="#">Privacy Policy</a></li>
                </ul>
            </div>
        </div>
    </div>
</footer>

<script src="<?php bloginfo('template_directory'); ?>/assets/js/vendor/jquery-1.11.2.min.js"></script>
<script src="<?php bloginfo('template_directory'); ?>/assets/js/vendor/bootstrap.min.js"></script>
<script src="<?php bloginfo('template_directory'); ?>/assets/js/plugins.js"></script>
<!-- Plugin JavaScript -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>
<script src="<?php bloginfo('template_directory'); ?>/assets/vendor/superslides/jquery.superslides.min.js"></script>
<script src="<?php bloginfo('template_directory'); ?>/assets/vendor/parallax.min.js"></script>
<script src="<?php bloginfo('template_directory'); ?>/assets/vendor/owl/owl.carousel.min.js"></script>
<!-- Theme JavaScript -->
<script src="<?php bloginfo('template_directory'); ?>/assets/js/main.js"></script>
</body>
</html>
