<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package cybyte
 */

get_header(); ?>
    <div class="page-title">
        <section class="breadcrumbs parallax-window text-center" data-parallax="scroll" data-image-src="<?php bloginfo('stylesheet_directory'); ?>/assets/img/brad2.jpg">
            <h2>Blog</h2>
            <div class="container">
                <div class="row">
                    <div class="col-sm-8 col-sm-offset-2">
                        <ol class="breadcrumb  breadcrumbs-path">
                            <li><a href="<?php echo home_url();?>">Home</a></li>
                            <li class="active">Blog</li>
                        </ol>
                    </div>
                </div>
        </section>
    </div>
    <div class="container">

    <div class="row nbox searched-for">
        <?php
        /* translators: %s: search query. */
        printf( esc_html__( 'Search Results for: %s', 'cybyte' ), '<span>' . get_search_query() . '</span>' );
        ?>
    </div>
        <div class="row">
            <section class="col-md-9" id="blog">
            <?php
            if ( have_posts() ) :
                /* Start the Loop */
                while ( have_posts() ) : the_post();

                    /**
                     * Run the loop for the search to output the results.
                     * If you want to overload this in a child theme then include a file
                     * called content-search.php and that will be used instead.
                     */
                    get_template_part( 'template-parts/content', 'search' );

                endwhile;

                the_posts_navigation();

                else :

                get_template_part( 'template-parts/content', 'none' );

                endif; ?>

            </section>
            <aside class="col-md-3">
                <?php get_sidebar(); ?>
            </aside>
        </div>
    </div>


<?php

get_footer();